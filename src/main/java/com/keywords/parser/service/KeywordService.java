package com.keywords.parser.service;

import com.keywords.parser.dto.SitesManager;

import java.util.List;

/**
 * <p>A service to resolve keywords from a site object.</p>
 * <p>
 * <p>Copyright © 2016 Rubicon Project, All rights reserved.</p>
 */
public interface KeywordService {

    /**
     * Resolves a list of keywords associated with a site.
     *
     * @param site
     * @return a comma delimited string or an empty string if there are no keywords associated with the site.
     */
    String resolveKeywords(String site);

    /**
     * Resolves a list of sites associated with a files.
     *
     * @param pathToDirectory
     * @return list of file managers.
     */
    List<SitesManager> parseKeywords(String pathToDirectory);

    /**
     * Saves information about sites in text file
     *
     * @param sitesManagers
     * @param filePath
     */
    void saveSiteManagers(List<SitesManager> sitesManagers, String filePath);

}