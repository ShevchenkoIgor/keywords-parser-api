package com.keywords.parser.service;

import com.keywords.parser.dto.Site;
import com.keywords.parser.dto.SitesManager;
import com.keywords.parser.parsers.IParser;
import com.keywords.parser.parsers.ParserCSV;
import com.keywords.parser.parsers.ParserJSON;
import com.keywords.parser.utils.KeywordsUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

/**
 * Service to resolve keywords from json, csv files and some site url
 *
 * @author Shevchenko Igor
 * @since 2016-07-15
 */
@Service
public class KeywordsParserService implements KeywordService {
    private static final Logger LOGGER = LoggerFactory.getLogger(KeywordsParserService.class);

    private PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver;
    private IParser parserCSV;
    private IParser parserJSON;

    @Autowired
    public KeywordsParserService(PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver,
                                 ParserCSV parserCSV, ParserJSON parserJSON) {
        this.pathMatchingResourcePatternResolver = pathMatchingResourcePatternResolver;
        this.parserCSV = parserCSV;
        this.parserJSON = parserJSON;
    }

    @Override
    public List<SitesManager> parseKeywords(String pathToDirectory) {
        LOGGER.info("Keywords parsing was started...");

        File fileCSV = getFullFilePath("file:" + pathToDirectory + "/*.csv");
        File fileJSON = getFullFilePath("file:" + pathToDirectory + "/*.json");

        List<SitesManager> sitesManagers = new ArrayList<>();
        SitesManager managerCSV = new SitesManager(fileCSV.getName(), new ArrayList<>());
        SitesManager managerJSON = new SitesManager(fileJSON.getName(), new ArrayList<>());

        managerCSV.getSites().addAll(parserCSV.getSites(fileCSV.getPath()));
        managerJSON.getSites().addAll(parserJSON.getSites(fileJSON.getPath()));

        sitesManagers.add(managerCSV);
        sitesManagers.add(managerJSON);

        setKeywords(sitesManagers);

        LOGGER.info("Keywords parsing was finished successfully.");
        return sitesManagers;
    }

    private File getFullFilePath(String path) {
        File file = null;
        try {
            Resource[] resource = pathMatchingResourcePatternResolver.getResources(path);
            file = resource[0].getFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    private void setKeywords(List<SitesManager> sitesManagerList) {
        for (SitesManager sitesManager : sitesManagerList) {
            for (Site site : sitesManager.getSites()) {
                String siteName = "http://" + site.getName();
                String htmlString = "";

                try {
                    LOGGER.info("Start reading site {}...", siteName);
                    Document doc = Jsoup.connect(siteName).get();
                    htmlString = doc.html();
                    LOGGER.info("Reading reading site {} was finished successfully.", site);
                } catch (IOException e) {
                    LOGGER.error("Site {} can't be opened", site, e);
                }

                site.setKeywords(resolveKeywords(htmlString));
            }
        }
    }

    @Override
    public void saveSiteManagers(List<SitesManager> sitesManagers, String filePath) {
        String sitesManagersString = KeywordsUtil.convertObjectToJsonString(sitesManagers);
        LOGGER.info(sitesManagersString);

        try {
            assert sitesManagersString != null;
            Files.write(Paths.get(filePath), sitesManagersString.getBytes(), StandardOpenOption.CREATE);
        } catch (IOException exc) {
            LOGGER.error("Error on reading file = {} ", filePath, exc);
        }
    }

    /**
     * Method which resolves keywords by tag <meta name='keywords' content='sport, tennis, football'/>
     *
     * @param site This is the string value of html
     * @return String This returns resolved keywords
     */
    @Override
    public String resolveKeywords(String site) {
        String keywords = "";
        if (!site.isEmpty()) {
            Document doc = Jsoup.parse(site);
            Elements link = doc.select("meta[name=keywords]");

            if (link != null) {
                keywords = link.attr("content");
            }
        }
        return keywords;
    }
}
