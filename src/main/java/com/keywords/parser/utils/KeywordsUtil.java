package com.keywords.parser.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * Util class for converting objects
 *
 * @author Shevchenko Igor
 * @since 2016-07-16
 */
public class KeywordsUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(KeywordsUtil.class);

    private static ObjectMapper mapper;

    @PostConstruct
    public void init() {
        mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    public static String convertObjectToJsonString(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        if (object == null) {
            return null;
        }
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        } catch (IOException exc) {
            LOGGER.error("Error on reading object " + object, exc);
            throw new RuntimeException(exc);
        }
    }
}
