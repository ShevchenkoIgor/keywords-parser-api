package com.keywords.parser.dto;

import java.util.List;

/**
 * DTO object to store site collection with id
 *
 * @author  Shevchenko Igor
 * @since   2016-07-15
 */
public class SitesManager {

    private String collectionId;
    private List<Site> sites;

    public SitesManager() {
    }

    public SitesManager(String collectionId, List<Site> sites) {
        this.collectionId = collectionId;
        this.sites = sites;
    }

    public String getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(String collectionId) {
        this.collectionId = collectionId;
    }

    public List<Site> getSites() {
        return sites;
    }

    public void setSites(List<Site> sites) {
        this.sites = sites;
    }
}
