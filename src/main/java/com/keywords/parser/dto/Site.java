package com.keywords.parser.dto;

/**
 * DTO object to store site information
 *
 * @author  Shevchenko Igor
 * @since   2016-07-15
 */
public class Site {
    private String id;
    private String name;
    private int mobile;
    private String keywords;
    private double score;

    public Site() {
    }

    public Site(String id, String name, int mobile, String keywords, double score) {
        this.id = id;
        this.name = name;
        this.mobile = mobile;
        this.keywords = keywords;
        this.score = score;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMobile() {
        return mobile;
    }

    public void setMobile(int mobile) {
        this.mobile = mobile;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
