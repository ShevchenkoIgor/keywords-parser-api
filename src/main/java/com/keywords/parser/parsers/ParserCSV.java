package com.keywords.parser.parsers;

import com.keywords.parser.dto.Site;
import com.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class to parse csv file according to path from command line arguments
 *
 * @author  Shevchenko Igor
 * @since   2016-07-15
 */
@Component
public class ParserCSV implements IParser{
    private static final Logger LOGGER = LoggerFactory.getLogger(ParserCSV.class);

    public List<Site> getSites(String filePath) {
        List<Site> sites = new ArrayList<>();

        try {
            LOGGER.info("Start reading CSV file...");
            CSVReader reader = new CSVReader(new FileReader(filePath), ',');
            List<String[]> csvEntries = reader.readAll();
            Iterator<String[]> iterator = csvEntries.iterator();
            iterator.next();

            while (iterator.hasNext()) {
                String[] row = iterator.next();
                if (row.length == 4) {
                    Site site = new Site(row[0], row[1], row[2].equals("true") ? 1 : 0, null, Double.parseDouble(row[3]));
                    sites.add(site);
                } else {
                    break;
                }
            }
            LOGGER.info("Reading SCV file was finished successfully.");
        } catch (IOException exc) {
            LOGGER.error("Error on reading file = {} ", filePath, exc);
        }

        return sites;
    }
}
