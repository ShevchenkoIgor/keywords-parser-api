package com.keywords.parser.parsers;

import com.keywords.parser.dto.Site;

import java.util.List;

public interface IParser {
    List<Site> getSites(String filePath);
}
