package com.keywords.parser.parsers;

import com.keywords.parser.dto.Site;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.toIntExact;

/**
 * Class to parse json file according to path from command line arguments
 *
 * @author  Shevchenko Igor
 * @since   2016-07-15
 */
@Component
public class ParserJSON implements IParser{
    private static final Logger LOGGER = LoggerFactory.getLogger(ParserJSON.class);

    public List<Site> getSites(String filePath) {
        List<Site> sites = new ArrayList<>();

        JSONParser parser = new JSONParser();

        try {
            LOGGER.info("Start reading JSON file...");
            JSONArray jsonObjects = (JSONArray) parser.parse(new FileReader(filePath));

            for (Object obj : jsonObjects) {
                JSONObject jsonObject = (JSONObject) obj;

                String siteId = (String) jsonObject.get("site_id");
                String name = (String) jsonObject.get("name");
                Integer mobile = toIntExact((Long) jsonObject.get("mobile"));
                Long score = (Long) jsonObject.get("score");

                Site site = new Site(siteId, name, mobile, null, score.doubleValue());
                sites.add(site);
            }
            LOGGER.info("Reading JSON file was finished successfully.");
        } catch (ParseException | IOException exc) {
            LOGGER.error("Error on reading file = {} ", filePath, exc);
        }
        return sites;
    }
}
