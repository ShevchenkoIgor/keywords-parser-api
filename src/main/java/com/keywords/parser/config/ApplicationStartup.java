package com.keywords.parser.config;

import com.keywords.parser.dto.SitesManager;
import com.keywords.parser.service.KeywordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Application listener to start KeywordsParserService class
 *
 * @author Shevchenko Igor
 * @since 2016-07-15
 */
@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationStartup.class);

    @Value("${pathToDirectory}")
    private String pathToDirectory;

    @Value("${outputFile}")
    private String outputFile;

    @Autowired
    private KeywordService keywordsParserService;

    /**
     * This event is executed as late as conceivably possible to indicate that
     * the application is ready to service requests.
     */
    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {

        List<SitesManager> sitesManagerList = keywordsParserService.parseKeywords(pathToDirectory);
        keywordsParserService.saveSiteManagers(sitesManagerList, outputFile);

    }
}
