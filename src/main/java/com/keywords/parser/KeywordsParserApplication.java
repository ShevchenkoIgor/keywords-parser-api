package com.keywords.parser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

/**
 * Main class to start keywords parser application
 *
 * @author  Shevchenko Igor
 * @since   2016-07-15
 */
@SpringBootApplication
public class KeywordsParserApplication {



    public static void main(String[] args){
        SpringApplication.run(KeywordsParserApplication.class, args);

    }

    @Bean
    public PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver(){
        return new PathMatchingResourcePatternResolver();
    }


}
