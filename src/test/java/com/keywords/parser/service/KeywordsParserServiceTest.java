package com.keywords.parser.service;


import com.google.common.io.CharStreams;
import com.keywords.parser.dto.Site;
import com.keywords.parser.dto.SitesManager;
import com.keywords.parser.parsers.IParser;
import com.keywords.parser.parsers.ParserCSV;
import com.keywords.parser.parsers.ParserJSON;
import org.junit.*;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class KeywordsParserServiceTest {

    @Autowired
    private KeywordsParserService keywordsParserService;
    @Autowired
    private PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver;
    @Autowired
    private IParser parserCSV;
    @Autowired
    private IParser parserJSON;

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    @Value("classpath:sample-data/html.txt")
    private Resource htmlSiteRecource;

    private List<Site> sites;


    @Before
    public void setup() throws IOException {

        sites =  new ArrayList<>();
        sites.add(new Site("12000", "example.com/csv1", 1, null, 454.00));
        sites.add(new Site("12001", "tsn.ua", 1, null, 128.00));

        Mockito.when(parserCSV.getSites(Mockito.anyString())).thenReturn(sites);
        Mockito.when(parserJSON.getSites(Mockito.anyString())).thenReturn(sites);

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource[] resources = resolver.getResources("classpath:input/*");
        Mockito.when(pathMatchingResourcePatternResolver.getResources(Mockito.anyString())).thenReturn(resources);
    }

    @After
    public void verify() throws IOException {
        Mockito.reset(pathMatchingResourcePatternResolver);
        Mockito.reset(parserCSV);
        Mockito.reset(parserJSON);
    }

    @Test()
    public void parseKeywordsTest() {
        List<SitesManager> sitesManagerList = keywordsParserService.parseKeywords("classpath:input/*");
        assertNotNull(sitesManagerList);
        assertTrue(sitesManagerList.size() == 2);
        assertEquals("", sitesManagerList.get(0).getSites().get(0).getKeywords());
        assertTrue(!sitesManagerList.get(0).getSites().get(1).getKeywords().isEmpty());
    }

    @Test()
    public void saveSiteManagersTest() throws IOException {
        List<SitesManager> sitesManagers = new ArrayList<>();
        sitesManagers.add(new SitesManager("test_id", sites));

        File tempFile = testFolder.newFile("file.txt");
        assertTrue(tempFile.length() == 0);

        keywordsParserService.saveSiteManagers(sitesManagers, tempFile.getPath());

        assertTrue(tempFile.exists());
        assertTrue(tempFile.length() > 0);
    }

    @Test()
    public void resolveKeywordsTest() throws IOException {
        String htmlSite = CharStreams.toString(new InputStreamReader(htmlSiteRecource.getInputStream()));
        String expected ="news, sport news, football, tennis, tenis news";
        String actual = keywordsParserService.resolveKeywords(htmlSite);

        assertEquals(expected, actual);
    }

    @Configuration
    static class KeywordsParserServiceTestContextConfiguration {

        @Bean
        public KeywordsParserService keywordsParserService() {
            return new KeywordsParserService(pathMatchingResourcePatternResolver(),
                    parserCSV(), parserJSON());
        }

        @Bean
        public PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver() {
            return Mockito.mock(PathMatchingResourcePatternResolver.class);
        }

        @Bean
        public ParserCSV parserCSV(){
            return Mockito.mock(ParserCSV.class);
        }

        @Bean
        public ParserJSON parserJSON(){
            return Mockito.mock(ParserJSON.class);
        }
    }
}
