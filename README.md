**Checkout:**

Attached jar file target\keywords-parser-api.jar to run application.

Copy jar to any place.

**Run and Test Spring boot application**

Create anywhere on file system **input** (put input1.csv and input2.json files) and **output** directories

run _**java -jar {jar_path}/keywords-parser-api.jar --pathToDirectory="D:\Test\InTechSoft\input" --outputFile="D:\Test\InTechSoft\output\output.txt"**_

Variables pathToDirectory and outputFile are configurable.

Application searches keyword in real web sites (i.e http://tsn.ua/ ) by **meta** tag and attribute **keywords** like

_<meta name='keywords' content='новини, новини ТСН, новини України, новини 1+1, 1+1, останні новини, новини дня, новини світу, новини україни сьогодні, новини києва' />_

Application can be tested with tests: KeywordsParserServiceTest

run tests